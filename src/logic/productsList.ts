import { List } from 'immutable';
import { Dispatch } from 'redux';

// Types
export const types = {
  FETCH_PRODUCTS: 'products/FETCH_PRODUCTS',
  FETCH_PRODUCTS_ERROR: 'products/FETCH_PRODUCTS_ERROR',
};

// Actions

// Operations
export const operations = {

  fetchProducts: () => (dispatch: Dispatch) => {
    fetch('/api/product')
      .then((res) => res.json())
      .then((payload) => {
        dispatch({
          payload,
          type: types.FETCH_PRODUCTS,
        });
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: types.FETCH_PRODUCTS_ERROR,
        });
      });
  },

};

// Reducer
export default function(state = List(), action: any) {
  switch (action.type) {
    case types.FETCH_PRODUCTS:
      return List(action.payload);
    case types.FETCH_PRODUCTS_ERROR:
      return action.payload;
    default:
      return state;
  }
}
