import { Record } from 'immutable';
import { Dispatch } from 'redux';

export class Profile extends Record({
  id: '',
  username: '',
  email: '',
  createdAt: 0,
}) {}

export const types = {
  FETCH: 'profile/FETCH',
};

type Action =
  { type: string, payload: Profile };

export const operations = {
  fetchUserInfo: (id: string) => (dispatch: Dispatch) => {
    fetch(`/api/user/${id}`)
      .then(res => res.json())
      .then((payload) => {
        dispatch({
          type: types.FETCH,
          payload,
        });
      });
  },
};

export default function (state = new Profile(), action: Action): Profile {
  switch (action.type) {
    case types.FETCH:
      return new Profile(action.payload);
    default:
      return state;
  }
}
