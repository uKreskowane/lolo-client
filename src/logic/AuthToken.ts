import { Record } from 'immutable';
import * as jwtDecode from 'jwt-decode';

export class Payload extends Record({
  exp: undefined,
  iat: undefined,
  id: undefined,
}) {}

export default class AuthToken extends Record({
  data: new Payload({}),
  jwt: '',
}) {
  public static existsInStore(): boolean {
    return localStorage.getItem('jwt') !== null;
  }

  public data: Payload;
  public jwt: string;

  constructor(newToken: string | null) {
    if (newToken !== null) {
      localStorage.setItem('jwt', newToken);
      super({
        data: new Payload(newToken ? jwtDecode(newToken) : {}),
        newToken,
      });
    } else if (AuthToken.existsInStore()) {
      const jwt = localStorage.getItem('jwt');
      super({
        data: new Payload(jwt ? jwtDecode(jwt) : {}),
        jwt,
      });
    } else {
      super();
    }
  }

  public removeToken(): void {
    localStorage.removeItem('jwt');
  }
}
