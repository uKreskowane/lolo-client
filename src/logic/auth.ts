import { Dispatch } from 'redux';
import AuthToken from './AuthToken';

type Actions = { type: string, payload: AuthToken };

export const types = {
  SIGN_IN: 'auth/SIGN_IN',
  SIGN_OUT: 'auth/SIGN_OUT',
};

export const actions = {
  signOut: {
    type: types.SIGN_OUT,
  },
};

export const operations = {
  signIn: (username: string, password: string) =>  async (dispatch: Dispatch) => {
    const response = await fetch(`/api/user/login`, {
      body: JSON.stringify({ username, password }),
      method: 'PATCH',
    });

    if (response.ok === false) {
      return;
    }

    const json = await response.json();

    dispatch({
      payload: json,
      type: types.SIGN_IN,
    });
  },
};

export default function(state = new AuthToken(null), action: Actions) {
  switch (action.type) {
    case types.SIGN_IN:
      return new AuthToken(action.payload.jwt);
    case types.SIGN_OUT:
      state.removeToken();
      return new AuthToken(null);
    default:
      return state;
  }
}
