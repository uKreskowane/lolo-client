import { UploadFile } from 'antd/lib/upload/interface';
import { Record } from 'immutable';
import { Dispatch } from 'redux';

const headers = new Headers({
  Authorization: 'Bearer ' + localStorage.getItem('jwt'),
});

export class Product extends Record({
  description: '',
  id: '',
  name: '',
  price: 0,
}) {}

// Types
export const types = {
  FETCH: 'product/FETCH',
};

type Actions = { type: string, payload: Product };

// Operations
export const operations = {
  fetch: (id: string) => (dispatch: Dispatch) => {
    fetch(`/api/product/${id}`)
      .then((res) => res.json())
      .then((payload) => {
        dispatch({
          payload,
          type: types.FETCH,
        });
      });
  },

  create: async (product: Product, images: any[]) => {
    const form = new FormData();

    form.append('name', product.get('name'));
    form.append('description', product.get('description'));
    form.append('price', product.get('price'));
    for (const image of images) {
      form.append('images', image);
    }

    const response = await fetch(`/api/product`, {
      body: form,
      headers,
      method: 'POST',
    });

  },
};

// Reducer
export default function(state = new Product(), action: Actions) {
  switch (action.type) {
    case types.FETCH:
      return new Product(action.payload);
    default:
      return state;
  }
}
