// @flow
import { Layout } from 'antd';
import * as React from 'react';
import Navbar from './ui/common/Navbar';

import product from './logic/product';

const { Content, Footer } = Layout;

interface IProps {
  children: React.ReactChild;
}

function App({ children }: IProps) {
  return (
    <Layout>
      <Navbar />
      <Content className='container'>
        {children}
      </Content>
      <Footer>
        <span>
          Lolo &copy;
        </span>
      </Footer>
    </Layout>
  );
}

export default App;
