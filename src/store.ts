import {
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import auth from './logic/auth';
import product from './logic/product';
import products from './logic/productsList';
import profile from './logic/profile';

export default createStore(

  combineReducers({
    auth,
    product,
    products,
    profile,
  }),

  applyMiddleware(thunk, logger),

);
