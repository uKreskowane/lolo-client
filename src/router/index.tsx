import * as React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import App from '../App';

import Auth from '../ui/views/Auth';
import CreateProduct from '../ui/views/CreateProduct';
import Product from '../ui/views/Product';
import ProductsList from '../ui/views/ProductsList';
import Profile from '../ui/views/Profile';
import SignOut from './SignOut';

export default function() {
  return (
    <Router>
      <App>
        <Switch>
          <Route path='/products/create' component={CreateProduct} />
          <Route path='/products/:id' component={Product} />
          <Route path='/products' component={ProductsList} />
          <Route path='/profile/:id' component={Profile} />
          <Route path='/auth/sign-out' component={SignOut} />
          <Route path='/auth' component={Auth} />
        </Switch>
      </App>
    </Router>
  );
}
