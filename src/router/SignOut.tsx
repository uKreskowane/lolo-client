import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { actions } from '../logic/auth';

interface IProps {
  signOut: () => void;
}

interface IState {
  redirect: boolean;
}

class SignOut extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      redirect: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState(() => ({ redirect: true }));
    }, 5000);
    this.props.signOut();
  }

  render() {
    const { redirect } = this.state;

    if (redirect) {
      return <Redirect to='/auth' />
    }

    return (
      <div>
        <h1>
          Zostałeś wylogowany
        </h1>
        <p>
          Za chwilę zostaniesz przekierowany na stronę logowania.
        </p>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    signOut: () => dispatch(actions.signOut),
  };
}

export default connect(undefined, mapDispatchToProps)(SignOut);
