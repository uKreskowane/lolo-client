import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import './stylesheets/index.less';

import App from './App';
import Router from './router';
import store from './store';

const renderTarget = document.getElementById('render-target');

if (renderTarget !== null) {
  render(
    <Provider store={store}>
      <Router />
    </Provider>,
    renderTarget,
  );
} else {
  document.write('<h1>Error has been occured.</h1>');
  document.write('<h2>Check if element "#render-target" exists in your HTML document</h2>');
}
