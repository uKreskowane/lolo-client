import { Layout, Menu } from 'antd';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AuthToken, { Payload } from '../../../logic/AuthToken';
import './style.less';
import UserController from './UserController';

const { Header } = Layout;
const { Item } = Menu;

interface IProps {
  me: Payload;
}

function NavBar({ me }: IProps) {
  return (
    <Header className='Navbar'>
      <Link to='/'>
        <h1>
          Lolo
        </h1>
      </Link>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <Menu
          mode='horizontal'
          theme='light'
          className='Navbar__menu'
        >
          <Item>
            <Link to='/products/create'>
              Dodaj ogłoszenie
            </Link>
          </Item>
          <Item>
            <Link to='/products'>
              Produkty
            </Link>
          </Item>
          {me.get('id') === undefined && <Item><Link to='/auth'>Zaloguj</Link></Item>}
        </Menu>
        {me.get('id') && <UserController />}
      </div>
    </Header>
  );
}

function mapStateToProsp(state: { auth: AuthToken }) {
  const me = state.auth.get('data');
  return {
    me,
  };
}

export default connect(mapStateToProsp)(NavBar);
