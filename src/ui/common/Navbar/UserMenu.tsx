import { Icon, Menu } from 'antd';
import * as React from 'react';
import { Link } from 'react-router-dom';

const UserMenu = (
  <Menu>
    <Menu.Item>
      <Link  to='/profile'>
        <Icon type='info' /> Profil
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to='/settings'>
        <Icon type='setting' /> Ustawienia
      </Link>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item>
      <Link to='/auth/sign-out'>
        <Icon type='logout' /> Wyloguj
      </Link>
    </Menu.Item>
  </Menu>
);

export default UserMenu;
