import { Button, Dropdown } from 'antd';
import * as React from 'react';
import UserMenu from './UserMenu';

function UserController() {
  return (
    <div className='UserController'>
      <Button icon='message' shape='circle' />
      <Dropdown overlay={UserMenu} trigger={['click']}>
        <Button icon='user' shape='circle' className='UserController__button' />
      </Dropdown>
    </div>
  );
}

export default UserController;
