import {
  Avatar,
  Button,
  Col,
  Row,
} from 'antd';
import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Profile as ProfileItem, operations } from '../../../logic/profile';
import './style.less';

interface IProps extends RouteComponentProps<any> {
  profile: ProfileItem;
  fetchUserInfo: (id: string) => void;
}

class Profile extends React.Component<IProps> {
  componentDidMount() {
    const {
      fetchUserInfo,
      match: {
        params: {
          id,
        },
      },
    } = this.props;

    fetchUserInfo(id);
  }

  render() {
    const { profile } = this.props;

    return (
      <div className='Profile'>
        <Row type='flex' align='middle'>
          <Avatar shape='square' icon='user' size={64} />
          <h1 className='Profile__username'>
            {profile.get('username')}
          </h1>
        </Row>
        <Row>
          <Button icon='mail'>
            Prywatna wiadomość
          </Button>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    fetchUserInfo: (id: string) => dispatch(operations.fetchUserInfo(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
