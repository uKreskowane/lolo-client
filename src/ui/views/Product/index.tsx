import {
  Button,
  Carousel,
  Col,
  Row,
} from 'antd';
import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import { Product as ProductType, operations } from '../../../logic/product';

interface IProps extends RouteComponentProps<any> {
  product: ProductType;
  fetch: (id: string) => void;
}

function mapStateToProps(state: any) {
  return {
    product: state.product,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    fetch: (id: string) => dispatch(operations.fetch(id)),
  };
}

class Product extends React.Component<IProps> {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetch(id);
  }

  public render() {
    const { product } = this.props;
    return (
      <div>
        <Row type='flex' justify='space-between'>
          <Col>
            <h1>
              {product.get('name')}
            </h1>
          </Col>
          <Col>
            <h2>
              {product.get('price').toFixed(2)} PLN
            </h2>
          </Col>
        </Row>
        <Row>
          <Button type='primary'>
            Kup
          </Button>
        </Row>
        <Header>
          Grafiki:
        </Header>
        <Row>
          <Carousel>
            <ExampleImage idx={1} />
            <ExampleImage idx={2} />
            <ExampleImage idx={3} />
          </Carousel>
        </Row>
        <Header>
          Opis:
        </Header>
        <Row>
          <p>
            {product.get('description')}
          </p>
        </Row>
      </div>
    );
  }
}

function Header(props: { children: string }) {
  return (
    <Row style={{ marginTop: 25 }}>
      <h2>
        {props.children}
      </h2>
    </Row>
  );
}

function ExampleImage(props: { idx: number }) {
  return (
    <div className='ExampleImage'>
      <h1>Image {props.idx}</h1>
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Product);
