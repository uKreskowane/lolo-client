import {
  Button,
  Form,
  Icon,
  Input,
  InputNumber,
  Upload,
} from 'antd';
import { FormComponentProps } from 'antd/lib/form/Form';
import { UploadFile } from 'antd/lib/upload/interface';
import * as React from 'react';
import { connect } from 'react-redux';
import { operations, Product } from '../../../logic/product';
import './style.less';

interface IProps extends FormComponentProps {
  create: (product: Product, images: UploadFile[]) => void;
}

interface IState {
  fileList: UploadFile[];
}

class CreateProduct extends React.Component<IProps, IState> {
  private static rules = {
    price: [
      { required: true, message: 'Podaj cenę' },
      { type: 'number', message: 'Wprowadź poprawną liczbę' },
      { validator: CreateProduct.isNegativeNumber, message: 'Dopłacanie to słaby biznes' },
    ],
    title: [
      { required: true, message: 'Wprowadź tytuł' },
    ],
  };

  private static formItemLayout = {
    labelCol: {
      sm: { span: 4 },
      xs: { span: 24 },
    },
    wrapperCol: {
      sm: { span: 16 },
      xs: { span: 24 },
    },
  };

  private static isNegativeNumber(rule: any, value: any, next: any) {
    if (value < 0) {
      next(new Error());
    }
    next();
  }

  public constructor(props: IProps) {
    super(props);

    this.state = {
      fileList: [],
    };

    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.handleRemoveFile = this.handleRemoveFile.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    const { rules } = CreateProduct;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>

        <h1>
          Dodaj ogłoszenie
        </h1>

        <Form.Item label='Tytuł' {...CreateProduct.formItemLayout}>
          {getFieldDecorator('name', { rules: rules.title })(
            <Input
              placeholder='Wprowadź tytuł'
            />)}
        </Form.Item>

        <Form.Item label='Cena (PLN)' {...CreateProduct.formItemLayout}>
          {getFieldDecorator('price', { rules: rules.price })(
            <InputNumber
              placeholder='Podaj oczekiwaną cenę'
              className='CreateProduct__input-number'
              min={0}
              step={10}
            />)}
        </Form.Item>

        <Form.Item label='Opis' {...CreateProduct.formItemLayout}>
          {getFieldDecorator('description')(
            <Input.TextArea
              placeholder='Wprowadź opis produktu'
              autosize={{ minRows: 4 }}
            />)}
        </Form.Item>

        <Form.Item label='Zdjęcia podglądowe' {...CreateProduct.formItemLayout}>
          {getFieldDecorator('images')(
            <Upload
              listType='picture-card'
              className='UploadImages'
              beforeUpload={this.handleChangeFile}
              onRemove={this.handleRemoveFile}
            >
              <Icon type='plus' className='UploadImages__icon' />
              <div className='UploadImages__text'>Dodaj grafikę</div>
            </Upload>)}
        </Form.Item>

        <Form.Item wrapperCol={{ sm: { offset: 4 } }}>
          <Button type='primary' htmlType='submit'>
            Dodaj
          </Button>
        </Form.Item>

      </Form>
    );
  }

  private handleChangeFile(file: UploadFile): false {
    this.setState((state: IState) => ({
      fileList: [
        ...state.fileList,
        file,
      ],
    }));

    return false;
  }

  private handleRemoveFile(file: UploadFile): void {
    this.setState((state: IState) => {
      const index = state.fileList.indexOf(file);
      const fileList = state.fileList.slice();
      fileList.splice(index, 1);
      return { fileList };
    });
  }

  private handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    const { create } = this.props;
    const { validateFields } = this.props.form;
    const { fileList } = this.state;

    event.preventDefault();

    validateFields((err, values) => {
      if (err) {
        return;
      }
      const product = new Product(values);
      create(product, fileList);
    });
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    create: (product: Product, images: UploadFile[]) => dispatch(operations.create(product, images)),
  };
}

export default connect(
  null,
  mapDispatchToProps,
)(
  Form.create()
(
  CreateProduct,
));
