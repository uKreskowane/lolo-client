import { Avatar, List } from 'antd';
import * as React from 'react';
import { Link } from 'react-router-dom';

import IProduct from '../../../models/IProduct';

const {
  Item,
  Item: { Meta },
} = List;

function Image() {
  return (
    <img
      height={95}
      alt='Logo example'
      src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1000px-React-icon.svg.png'
    />
  );
}

function Product(props: IProduct) {
  return (
    <Item
      extra={<Image />}
    >
      <Meta
        title={<Link to={`/products/${props.id}`}>{props.name}</Link>}
        description={props.description}
      />
      {props.price.toFixed(2)} PLN
    </Item>
  );
}

export default Product;
