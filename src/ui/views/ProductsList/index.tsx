import { List } from 'antd';
import { List as ImmutableList } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';

// @ts-ignore
import { operations } from '../../../logic/productsList';
import IProduct from '../../../models/IProduct';
import Product from './Product';

interface IProps {
  products: ImmutableList<IProduct>;
  fetchProducts: () => void;
}

function mapStateToProps(state: any) {
  return {
    products: state.products,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    fetchProducts: () => dispatch(operations.fetchProducts()),
  };
}

class ProductsList extends React.Component<IProps> {
  private static renderProducts(product: IProduct) {
    return (
      <Product
        id={product.id}
        key={product.id}
        name={product.name}
        description={product.description}
        price={product.price}
        is_active={product.is_active}
      />
    );
  }

  public componentDidMount() {
    const { fetchProducts } = this.props;
    fetchProducts();
  }

  public render() {
    const { products } = this.props;

    return (
      <div>
        <h2>
          Produkty
        </h2>

        <List
          itemLayout='vertical'
          size='large'
          dataSource={products}
          renderItem={ProductsList.renderProducts}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
