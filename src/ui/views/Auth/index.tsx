import {
  Button,
  Form,
  Icon,
  Input,
} from 'antd';
import { FormComponentProps } from 'antd/lib/form/Form';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { operations } from '../../../logic/auth';
import AuthToken from '../../../logic/AuthToken';

const FormItem = Form.Item;

interface IProps {
  auth: AuthToken;
  signIn: (username: string, password: string) => void;
}

interface IState {
  success: boolean;
}

class Auth extends React.Component<IProps & FormComponentProps, IState> {
  constructor(props: IProps & FormComponentProps) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    const { auth } = this.props;
    const { getFieldDecorator } = this.props.form;

    if (auth.get('data').get('id')) {
      return <Redirect to='/products' />;
    }

    return (
      <Form onSubmit={this.handleSubmit}>

        <h1>
          Logowanie
        </h1>

        <FormItem>
          {getFieldDecorator('username')(
            <Input
              prefix={<Icon type='user' style={{ opacity: 0.25 }} />}
              placeholder='Username'
            />)}
        </FormItem>

        <FormItem>
          {getFieldDecorator('password')(
            <Input
              prefix={<Icon type='lock' style={{ opacity: 0.25 }} />}
              type='password'
              placeholder='Hasło'
            />)}
        </FormItem>

        <Button type='primary' htmlType='submit'>
          Zaloguj
        </Button>

      </Form>
    );
  }

  private handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    const { signIn } = this.props;
    const { validateFields } = this.props.form;

    event.preventDefault();

    validateFields((err, values) => {
      if (err) {
        return;
      } else {
        const { username, password } = values;
        signIn(username, password);
      }
    });
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    signIn: (username: string, password: string) =>
      dispatch(operations.signIn(username, password)),
  };
}

export default Form.create()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    Auth,
  ));
